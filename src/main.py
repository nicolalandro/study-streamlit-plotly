import streamlit as st
import plotly.graph_objs as go
from ipywidgets import interactive, HBox, VBox

import pandas as pd
import numpy as np

st.title('Dyn Graph')

# PLOT
df = pd.read_csv('https://raw.githubusercontent.com/jonmmease/plotly_ipywidget_notebooks/master/notebooks/data/cars/cars.csv')

f = go.FigureWidget([go.Scatter(y = df['City mpg'], x = df['City mpg'], mode = 'markers')])
scatter = f.data[0]
N = len(df)
scatter.x = scatter.x + np.random.rand(N)/10 *(df['City mpg'].max() - df['City mpg'].min())
scatter.y = scatter.y + np.random.rand(N)/10 *(df['City mpg'].max() - df['City mpg'].min())
scatter.marker.opacity = 0.5

st.plotly_chart(f, use_container_width=True)
# That part cannot work https://discuss.streamlit.io/t/function-is-not-called/40796
# slot = st.empty()

# def selection_fn(trace,points,selector):
#     # [df.loc[points.point_inds][col] for col in ['ID','Classification','Driveline','Hybrid']]
#     print('hello')
#     slot.dataframe(df)
# scatter.on_selection(selection_fn)


