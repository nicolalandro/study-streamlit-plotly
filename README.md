# Study Plotly with Streamlit
This codebase is an example to use Plotly on streamlit

## How to run

```
# if you want to test agraph add `command: streamlit run src/main_agraph.py --server.port 8000` into docker-compose.yml
docker-compose up 
# go to localhost:8000
```

# References
* python
* docker, docker compose
* streamlit
  * [use empty or slot](https://docs.streamlit.io/knowledge-base/using-streamlit/insert-elements-out-of-order)
  * [use plotly](https://docs.streamlit.io/library/api-reference/charts/st.plotly_chart)
* plotly
  * [example of interactive](https://plotly.com/python/v3/selection-events/)
* [streamlit-agraph](https://github.com/ChrisDelClea/streamlit-agraph)